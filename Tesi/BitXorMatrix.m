@Override
    public Object runTrain(Object... args) {

        //Update the model status

        boolean result = (boolean) preProcessing();
        if(result){
            LinearRegressionModel lrModel = (LinearRegressionModel) train();
            if(lrModel != null){
                getApplication().getModelDescription().setStatus(MLTesiConstants.MODEL_STATUS_PROCESSING);
                DBUtils.updateApplication(getApplication());
                postProcessing(lrModel);
                setImplementedModel(lrModel);
//              save(lrModel);
            }
        }else{
            getApplication().getModelDescription().setStatus(MLTesiConstants.MODEL_STATUS_ERROR);
            DBUtils.updateApplication(getApplication());
        }

        return true;
    }