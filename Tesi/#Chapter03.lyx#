#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass classicthesis
\begin_preamble

\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding default
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Implementazione del Sistema
\end_layout

\begin_layout Standard
\begin_inset CommandInset label
LatexCommand label
name "ch:Implementazione del Sistema"

\end_inset

Una volta definita l'architettura, è possibile passare all'implementazione
 del software.
 Il linguaggio di programmazione utilizzato appartiene alla famiglia dei
 linguaggi basati sul paradigma OO (
\shape italic
Objected Oriented
\shape default
) e corrisponde a Java v1.8.
 
\end_layout

\begin_layout Section
Definizione Task
\end_layout

\begin_layout Standard
L'oggetto Task mostrato nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java ML Task}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java ML Task,label={lis:Java ML Task}]{cod
ici/Task.java} 
\end_layout

\end_inset

 rappresenta il problema di apprendimento automatico che deve essere risolto
 dal sistema.
 All'interno di questo oggetto ci sono altri due oggetti che rappresentano
 rispettivamente la descrizione del modello che risolve quel determinato
 task e le informazioni relative alla fase di predizione.
 Il listato seguente mostra i principali attributi dell'oggetto 
\shape italic
ModelDescription
\shape default
:
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Model Description,label={lis:Java
 Model Description}]{codici/ModelDescription.java} 
\end_layout

\end_inset

che rappresentano le informazioni descritte nella sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Creazione-del-modello"

\end_inset

 necessarie alla creazione del modello.
\end_layout

\begin_layout Standard
Nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Prediction Description}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Prediction Description,label={lis:Jav
a Prediction Description}]{codici/PredictionDescription.java} 
\end_layout

\end_inset

vengono mostrati gli attributi principali per la fase di predizione di tipo
 
\shape italic
Streaming 
\shape default
con
\shape italic
 
\shape default
modello
\shape italic
 offline
\shape default
 (vedi sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec: Creazione della fase di predizione"

\end_inset

).
\end_layout

\begin_layout Section
Costruzione Modelli di Machine Learning
\end_layout

\begin_layout Standard
I modelli che sono stati implementati nella prima versione del software
 sono elencati nella tabella 
\begin_inset CommandInset ref
LatexCommand ref
reference "tab:Algoritmi-di-ML-implementati"

\end_inset

.
 
\begin_inset Float table
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="3" columns="3">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Regressione
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Classificazione
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Clustering
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Linear Regreesion
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Logistic Regression
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
K-means
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
DecisionTree
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Support Vector Machine
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Latent Dirichlet allocation
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "tab:Algoritmi-di-ML-implementati"

\end_inset

Algoritmi di ML implementati
\end_layout

\end_inset


\end_layout

\end_inset

Come verrà spiegato in seguito, il sistema è stato pensato per l'inserimento
 di nuovi modelli senza dovere stravolgere l'architettura del sistema.
 MMETERE DESCRIZIONE LIBRERIE MLIB
\end_layout

\begin_layout Standard
Data la necessità di rendere il sistema indipendente dall'
\shape italic
engine
\shape default
 utilizzato per il processamento dei dati e per la costruzione dei modelli,
 la struttura di un modello di machine learning segue la gerarchia adottata
 in figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Gerarchia-Modello"

\end_inset

.
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename schemi/ClassDiagramModelPackage.png
	scale 60
	scaleBeforeRotation

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Gerarchia Modello
\begin_inset CommandInset label
LatexCommand label
name "fig:Gerarchia-Modello"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset

L'interfaccia 
\shape italic
ModelProcessing 
\shape default
definisce i metodi che un modello di Machine Learning deve implementare.
 Nello specifico questi metodi sono:
\end_layout

\begin_layout Itemize

\series bold
\shape italic
runTrain
\series default
\shape default
: esecuzione della logica per il processo di training (descritto nella sezione
 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Pattern Recognition"

\end_inset

)
\end_layout

\begin_layout Itemize

\series bold
\shape italic
predictionOn
\series default
\shape default
 : esegue la predizione a partire da un valore in input
\end_layout

\begin_layout Itemize

\series bold
\shape italic
preProcessing
\series default
\shape default
 : esegue il preprocessing del dataset per essere utilizzato per la fase
 di 
\shape italic
training
\end_layout

\begin_layout Itemize

\series bold
\shape italic
train
\series default
\shape default
: esecuzione vera e propria del processo di training
\end_layout

\begin_layout Itemize

\series bold
\shape italic
postProcessing
\series default
\shape default
: operazione di post processing.
 Questa operazione consiste nell'estrazione delle metriche di validazione
\end_layout

\begin_layout Itemize

\series bold
\shape italic
save
\series default
\shape default
: esecuzione del salvataggio del modello
\end_layout

\begin_layout Standard
La classe 
\shape italic
Model
\shape default
 rappresenta l'oggetto astratto Modello e viene 
\begin_inset Quotes eld
\end_inset

esteso
\begin_inset Quotes erd
\end_inset

 dalle classi astratte 
\shape italic
OfflineModel 
\emph on
e 
\shape default
OnlineModel
\shape italic
.
 Questa classi rappresentano il tipo di costruzione del modello: la prima
 viene usata per i modelli il cui training viene eseguito a partire da un
 dataset proveniente da un datasource statico, la seconda invece rappresenta
 i modelli la cui costruzione avviene attraverso un streaming dati online,
 ossia il 
\shape default
training 
\shape italic
viene fatto in maniera iterativa online (vedremo un esempio di questo tipo
 nelle prossime sezioni, va messo prima? boh).
 scrivere sull'implemented model
\end_layout

\begin_layout Standard
Le classi 
\shape italic
OfflineModel 
\emph on
e 
\shape default
OnlineModel
\emph default
 a loro volta vengono estese dalle classi che categorizzano il tipo di modello
 in accordo alle definizioni date nella sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Pattern Recognition"

\end_inset

.
 Nella figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Gerarchia-Modello"

\end_inset

 sono mostrate le classi astratte 
\shape italic
RegressionOfflineModel, ClassificationOfflineModel
\shape default
 per i modelli il cui 
\shape italic
training 
\shape default
viene effettuato offline.
 
\end_layout

\begin_layout Section
Implementazione del modello
\end_layout

\begin_layout Standard
In figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Implementazione-modelli-di-regression"

\end_inset

 è riportata la struttura gerarchica dell'implementazione di due modelli
 che risolvono problemi di regressione: il DecisionTree e la 
\shape italic
LinearRegression
\shape default
.
 
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename schemi/offlineModels.png
	scale 60
	rotateAngle 90

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Implementazione-modelli-di-regression"

\end_inset

Implementazione modelli di Regessione
\end_layout

\end_inset


\end_layout

\end_inset

In queste classi c'è l'implementazione dei metodi definiti all'interno dell'inte
rfaccia 
\shape italic
Model Processing
\shape default
.
 Vediamo ora in dettaglio la classe che rappresenta il modello della Linear
 Regression.
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Linear Regression Model,float=tb,labe
l={lis:Java Linear Regression Model}]{codici/LinearRegressionOfflineModel.java}
 
\end_layout

\end_inset

Come si evince dal listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Linear Regression Model}
\end_layout

\end_inset

 il metodo 
\shape italic
runTrain
\shape default
 inizializza il processo di 
\shape italic
training
\shape default
 del modello richiamando il metodo 
\shape italic
preprocessing
\shape default
, mostrato nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Preprocessing Data Set}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Preprocessing Model,float=tb,label={l
is:Java Preprocessing Data Set}]{codici/preprocessiing.java} 
\end_layout

\end_inset

All'interno di questo metodo viene caricato il dataset e vengono selezionate
 le features passate in input sulle quali deve essere costruito il modello.
 Una volta che il dataset è stato processato, è possibile passare alla fase
 di train vera e propria.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Train Model,float=tb,label={lis:Java
 Train model}]{codici/train.java} 
\end_layout

\end_inset

Nel metodo 
\shape italic
train
\shape default
, mostrato nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Train model}
\end_layout

\end_inset

, risiede la vera implementazione del modello.
 Nel nostro caso, il modello utilizzato è quello presente nelle librerie
 di Apache Spark e rappresenta il modello matematico della 
\shape italic
LinearRegression
\shape default
.
 Una volta che il modello è inizializzato è possibile eseguire il fitting
 del dataset sul modello attraverso la funzione fit().
\end_layout

\begin_layout Standard
\begin_inset VSpace smallskip
\end_inset

Dopo avere eseguito il fitting del modello vengono estratte e salvate le
 metriche di validazione attraverso il metodo di 
\shape italic
postProcessing
\shape default
; un esempio del metodo è mostrato nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java PostProcessing}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java PostProcessing,float=tb,label={lis:Ja
va PostProcessing}]{codici/postprocessing.java} 
\end_layout

\end_inset


\end_layout

\begin_layout Section
Scelta del modello
\end_layout

\begin_layout Standard
Il compito del sistema è quello di scegliere il modello che restituisce
 il risultato migliore in base a delle opportune metriche di validazione.
 Nella tabella 
\begin_inset CommandInset ref
LatexCommand ref
reference "tab:Tabella-delle-metriche"

\end_inset

 sono mostrate le diverse metriche utilizzate per la validazione dei modelli
 di regressione o di classificazione.
\begin_inset VSpace smallskip
\end_inset

Il processo di scelta del modello avviene tramite l'inizializzazione di
 un 
\shape italic
ModelPlan
\shape default
 il quale si occupa di gestire la creazione dei modelli appartenenti al
 tipo di categorizzazione del problema.
 L'implementazione del metodo segue il processo descritto nella figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Scelta del modello finale"

\end_inset

 mentre nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Model Plan}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Model Plan,float=tb,label={lis:Java
 Model Plan}]{codici/modelplan.java} 
\end_layout

\end_inset

viene mostrato un estratto della classe 
\shape italic
ModelPlan
\shape default
 che si occupa della esecuzione del processo di scelta di modelli per problemi
 di Regressione.
 Anche in questo caso è possibile evidenziare di come il sistema non conosce
 l'effettiva implementazione degli algoritmi utilizzati all'interno dei
 modelli.
 Le classi 
\shape italic
SVMClassificationOfflineModel 
\shape default
e 
\shape italic
LogisticRegressionOfflineModel 
\shape default
sono dei 
\shape italic
wrapper 
\shape default
per l'implementazione vera e propria del modello ossia dei modelli messi
 a disposizione di 
\shape italic
Apache Spark.
\end_layout

\begin_layout Section
Salvataggio del modello
\end_layout

\begin_layout Standard
Una volta scelto il miglio modello si procederà con il salvataggio di quest'ulti
mo contestualmente all'aggiornamento del valore che indica il modello utilizzato
 per risolvere il determinato task di ML passato in input al sistema.
 In questa prima implementazione del sistema, i modelli vengono salvati
 all'interno di un filesystem 
\shape italic
HDFS
\shape default
 di 
\shape italic
Apache Hadoop
\shape default
: questa scelta è stata adottata nell'ottica di operare all'interno di ambienti
 distribuiti che si basano su filesystem distribuiti.
 Ad ogni modo il salvataggio del modello è lasciato alla classe che implementa
 il reale modello in quanto ogni implementazione (nel nostro caso quella
 di Apache spark) può salvare i modelli in maniera differente.
 Anche in questo caso il sistema si preoccuperà solamente di chiamare il
 metodo 
\shape italic
saveModel()
\shape default
 appartenente al modello scelto durante la selezione dei modelli.
\end_layout

\begin_layout Section
Predizione
\end_layout

\begin_layout Standard
In accordo alla sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec: Creazione della fase di predizione"

\end_inset

 la predizione può avvenire in due modi differenti, la prima in maniera
 offline, la seconda in Streaming.
 E' utile esaminare la predizione in streaming in quanto la predizione offline
 è un particolare caso di quella streaming in quanto lo streaming può essere
 visto come una sorgente che produce un singolo dato durante il suo ciclo
 di vita.
 La struttura di un oggetto di predizione è mostrata nella figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Class-Diagram-Predizione"

\end_inset

 ed è simile a quella creata per i modelli.
 
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename schemi/PredictionsDiagram.png
	scale 60
	rotateAngle 90

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Class-Diagram-Predizione"

\end_inset

Class Diagram Predizione
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset

La classe 
\shape italic
Prediction
\shape default
 rappresenta l'oggetto astratto della predizione che implementa l'interfaccia
 
\shape italic
PredictionProcessing.
 
\shape default
Le classi 
\shape italic
StreamingPredictionOnLineModel 
\shape default
e 
\shape italic
StreamingPredictionOfflineModel
\shape default
 rappresentano la specializzazione della classe Prediction in merito alle
 predizioni con modello offline e con modello online rispettivamente.
 
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Subsection
Predizione con modello Offline
\end_layout

\begin_layout Standard
La logica per la creazione di una predizione con modello offline è mostrata
 nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Prediction offline}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java creazione predizione con modello
 offline,float=tb,label={lis:Java Prediction offline}]{codici/ManagePrediction.ja
va} 
\end_layout

\end_inset

Prima di iniziare la fase vera e propria di predizione, viene caricato il
 modello passato in input attraverso il metodo 
\shape italic
loadModel
\shape default
() del 
\shape italic
package
\shape default
 che si occupa della gestione dei modelli e successivamente sarà invocato
 il metodo 
\shape italic
runPredictionOn
\shape default
().
\begin_inset VSpace defskip
\end_inset

Nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Predictions offline model}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Predictions offline model,float=tb,la
bel={lis:Java Predictions offline model}]{codici/predictionsOffline.java}
 
\end_layout

\end_inset

viene mostrato il metodo 
\shape italic
PredictionOn() 
\shape default
della classe 
\shape italic
StreamingPredictionOfflineModel().
 
\shape default
Nella riga dieci del listata si può vedere come la classe è all'oscuro del
 modello che viene utilizzato per la predizione in quanto viene semplicemente
 invocato il 
\shape italic
getModel().predictionOn(dataFrame).
 
\shape default
Il metodo 
\shape italic
predictionOn()
\shape default
 sarà invocato sul modello che è stato caricato all'interno del 
\shape italic
loadModel(), 
\shape default
un esempio del metodo 
\shape italic
predictOn()
\shape default
 è mostrato nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Predictions on}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Predictions on,float=tb,label={lis:Ja
va Predictions on}]{codici/predictOn.java} 
\end_layout

\end_inset

che richiama il metodo 
\shape italic
getImplementedModel
\shape default
() per farsi restituire la reale implementazione del modello costruita attravers
o il 
\shape italic
loadModel
\shape default
() e su questa esegue il metodo 
\shape italic
transform
\shape default
() che esegue la predizione vera e propria.
\end_layout

\begin_layout Subsection
Predizione con modello Online
\end_layout

\begin_layout Standard
La creazione di una predizione con modello online è analoga a quanto avviene
 per quella con modello offline.
 La sostanziale differenza risiede nel caricamento del modello: in questo
 il modello non viene caricato da un 
\shape italic
datasource
\shape default
 bensì viene creato 
\shape italic
ex novo 
\shape default
in quanto il training di questo modello sarà eseguito in concomitanza con
 la fase di predizione.
\end_layout

\begin_layout Standard
Nel listato 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
ref{lis:Java Predictions offline model}
\end_layout

\end_inset

 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
lstinputlisting[language=Java,caption=Java Predictions offline model,float=tb,la
bel={lis:Java Predictions offline model}]{codici/predictionsOffline.java}
 
\end_layout

\end_inset

viene mostrato il metodo 
\shape italic
PredictionOn() 
\shape default
della classe 
\shape italic
StreamingPredictionOfflineModel()
\end_layout

\end_body
\end_document
