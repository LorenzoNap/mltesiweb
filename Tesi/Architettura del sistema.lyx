#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass classicthesis
\use_default_options true
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding default
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Architettura del sistema
\end_layout

\begin_layout Standard
\begin_inset CommandInset label
LatexCommand label
name "ch:examples"

\end_inset

In questo capitolo verrà definita l’architettura del sistema e i principali
 moduli che ne faranno parte.
 Questo processo verrà gestito tenendo a mente i requisiti del sistema.
\end_layout

\begin_layout Section
Requisiti del sistema
\end_layout

\begin_layout Standard
Prima di iniziare lo sviluppo dell’architettura del sistema è necessario
 comprendere e definire in maniera chiara e precisa quali sono i requisiti
 che il sistema deve soddisfare per ridurre gli errori durante la fase progettua
le dell’architettura.
 Avere una precisa dimensione dei requisiti del sistema obbliga l’architetto
 a valutare in modo efficiente i suoi punti critici e a scegliere un’architettur
a che, qualora dovessero essere apportati dei cambiamenti durante la fase
 di sviluppo del software o dopo che l’applicazione fosse rilasciata, supporti,
 a basso costo, operazioni di modifica del sistema stesso.
 I requisiti principali che il sistema deve soddisfare sono i seguenti:
\end_layout

\begin_layout Itemize
Scelta, in maniera efficiente, di un modello di ML che risolve un problema
 di apprendimento automatico passato in 
\shape italic
input;
\end_layout

\begin_layout Itemize
Gestione di 
\shape italic
dataset
\shape default
 situati su 
\shape italic
filesystem
\shape default
 distribuiti e non;
\end_layout

\begin_layout Itemize
Salvataggio dei modelli di ML su memoria persistente;
\end_layout

\begin_layout Itemize
Caricamento di un modello creato in precedenza per utilizzarlo in una fase
 di predizione;
\end_layout

\begin_layout Itemize
La fase di predizione può avvenire sia 
\shape italic
offline
\shape default
 che attraverso uno streaming 
\shape italic
online
\shape default
 di 
\shape italic
input;
\end_layout

\begin_layout Itemize
Salvataggio in maniera persistente di un 
\shape italic
task
\shape default
 di ML;
\end_layout

\begin_layout Standard
Vediamo ora in dettaglio i requisiti espressi nei precedenti punti.
\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard
Il compito principale del sistema consiste nel risolvere 
\shape italic
task
\shape default
 di 
\shape italic
Machine Learning
\shape default
 dove per 
\shape italic
task
\shape default
 si intende un problema di apprendimento automatico risolvibile tramite
 l'applicazione di un algoritmo basato su un modello matematico.
 Come descritto nel capitolo 
\begin_inset CommandInset ref
LatexCommand ref
reference "ch:Cap1-MachineLearning"

\end_inset

 il modello, una volta costruito, permette di effettuare delle predizioni
 su 
\shape italic
input
\shape default
 sconosciuti al sistema.
\end_layout

\begin_layout Standard
Considerando l'esistenza di molteplici soluzioni, ossia diversi modelli
 che risolvono lo stesso problema di ML, il sistema, in maniera automatica,
 deve essere in grado di scegliere il modello tra quelli implementati nel
 sistema che meglio si adatta al problema in 
\shape italic
input
\shape default
.
 La scelta del modello avverrà tramite una procedura automatica che, in
 base a delle opportune metriche di validazione, selezionerà il modello
 che restituirà il punteggio più alto di quest'ultime.
\end_layout

\begin_layout Standard
La fase preliminare per la costruzione di un modello di ML consiste nell'acquisi
zione e nel 
\shape italic
preprocessing 
\shape default
del 
\shape italic
dataset
\shape default
.
 Poiché uno degli obiettivi del sistema è quello di essere utilizzabile
 all'interno di ambienti distribuiti, verranno fornite delle API che permettano
 di operare con 
\shape italic
datasource
\shape default
 distribuiti e non.
 A tale scopo verrà utilizzato l'
\shape italic
engine
\shape default
 di 
\shape italic
Apache Spark
\shape default
 il quale mette a disposizione delle chiamate per interagire con 
\shape italic
datasource
\shape default
 distribuiti come 
\shape italic
Apache Hadoop
\shape default
, 
\shape italic
Cassandra, Hive
\shape default
 e con singoli file del tipo 
\shape italic
json
\shape default
 e 
\shape italic
csv
\shape default
.
 Una volta acquisito e processato il 
\shape italic
dataset
\shape default
 è possibile iniziare la fase di costruzione e di scelta del modello di
 ML, fase durante la quale verrano costruiti i modelli che possono essere
 utilizzati per risolvere il problema di ML passato in 
\shape italic
input
\shape default
 (come descritto nella sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Creazione-del-modello"

\end_inset

).
 Una volta selezionato il modello che meglio si adatta alla risoluzione
 del problema, questo viene salvato in memoria persistente per potere essere
 
\begin_inset Quotes eld
\end_inset

chiamato
\begin_inset Quotes erd
\end_inset

 ogni qual volta è necessario attuare una fase di predizione sulle istanze
 appartenenti al dominio sul quale è stato costruito il modello.
\end_layout

\begin_layout Standard
La fase di predizione consiste solamente nel caricare un modello precedentemente
 creato e utilizzarlo come funzione di predizione.
 La sorgente dati per l'
\shape italic
input
\shape default
 della funzione di predizione può essere di due tipi:
\end_layout

\begin_layout Itemize

\shape italic
Offline
\shape default
: viene processato un singolo 
\shape italic
input
\shape default
 e restituito il valore della predizione;
\end_layout

\begin_layout Itemize

\shape italic
Streaming Online
\shape default
: viene inviato un flusso continuo di 
\shape italic
input
\shape default
; per ogni 
\shape italic
input
\shape default
 processato verrà restituito il valore della funzione di predizione;
\end_layout

\begin_layout Standard
La predizione attraverso lo 
\shape italic
streaming online
\shape default
 risulta molto utile nel caso di applicazioni che richiedono risposte 
\shape italic
real-time
\shape default
: un esempio di integrazione tra questi sistemi e il sistema sviluppato
 sarà esaminata nel capitolo 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Integrazione con la piattaforma !CHAOS"

\end_inset

.
 L'
\shape italic
output
\shape default
 della predizione verrà salvato su memoria persistente affinché possa essere
 sfruttato da applicazioni da terze parti come 
\shape italic
Grafana
\shape default

\begin_inset CommandInset citation
LatexCommand citep
key "key-21"

\end_inset

 che permettono la costruzione di grafici e 
\shape italic
dashboard
\shape default
 per la visualizzazione di valori basati su 
\shape italic
time-series
\shape default
.
\end_layout

\begin_layout Standard
Durante il processo di acquisizione del 
\shape italic
dataset
\shape default
, della creazione dei modelli, della scelta finale del modello e della fase
 di predizione, il sistema deve essere in grado di salvare gli 
\shape italic
output
\shape default
 delle varie operazioni: in questo modo si può tenere traccia dello 
\shape italic
status 
\shape default
del 
\shape italic
task
\shape default
 di ML.
 Ad ogni 
\shape italic
task
\shape default
 sottomesso al sistema verranno associate, in accordo alla sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "ch:Task di Machine Learning"

\end_inset

, due informazioni principali: la prima descrive il modello finale associato
 a quel determinato 
\shape italic
task
\shape default
 e la seconda descrive il processo di predizione in corso (o l'ultimo eseguito)
 su quest'ultimo.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/System Stack.png
	scale 40

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout

\shape italic
System Stack
\shape default

\begin_inset CommandInset label
LatexCommand label
name "fig:System-Stack"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Section
System Stack
\end_layout

\begin_layout Standard
In figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:System-Stack"

\end_inset

 è rappresentato il 
\shape italic
System Stack
\shape default
 del sistema il quale fornisce una panoramica generale dei principali blocchi
 dell'architettura.
 Nello specifico sono:
\end_layout

\begin_layout Itemize

\shape italic
Spark Core
\shape default
: rappresenta il cuore dell'applicazione in quanto è l'
\shape italic
engine 
\shape default
utilizzato per il 
\shape italic
processing
\shape default
 dei 
\shape italic
dataset
\shape default
 e per l'implementazione dei modelli di 
\shape italic
Machine Learning;
\end_layout

\begin_layout Itemize

\shape italic
Spark DataSource
\shape default
: fornisce le API per accedere ai diversi 
\shape italic
datasource
\shape default
 distribuiti e non utilizzati da 
\shape italic
Spark;
\end_layout

\begin_layout Itemize

\shape italic
Machine Learning Model
\shape default
 
\shape italic
Layer
\shape default
: si occupa dell'astrazione dei modelli di ML e del loro ciclo di vita all'inter
no del sistema;
\end_layout

\begin_layout Itemize

\shape italic
System Data Layer
\shape default
: gestisce l'interazione tra il sistema e i diversi 
\shape italic
datasource
\shape default
 (
\shape italic
filesystem
\shape default
 distribuiti, 
\shape italic
database
\shape default
, ecc...);
\end_layout

\begin_layout Itemize

\shape italic
Machine Learning Task
\shape default
 
\shape italic
Layer
\shape default
: rappresenta l'
\shape italic
entry point
\shape default
 del sistema.
 Gestisce la logica per le varie operazioni messe a disposizione dal sistema;
\end_layout

\begin_layout Standard
Il blocco 
\shape italic
Spark Core
\shape default
 è costituito dall'insieme delle librerie messe a disposizione da 
\shape italic
Apache Spark 
\shape default
per il 
\shape italic
data-processing.
 
\shape default
L'utilizzo di queste librerie permette due importanti operazioni: la prima
 è quella di fornire delle API di alto livello per la creazione di oggetti,
 chiamati 
\shape italic
DataFrame
\shape default

\begin_inset CommandInset citation
LatexCommand citep
key "key-49"

\end_inset

, che incapsulano il 
\shape italic
dataset
\shape default
 al fine di renderlo facilmente accessibile e processabile; la seconda mette
 a disposizione dell'utente una serie di strumenti per la costruzione e
 la validazione di modelli di ML.
\end_layout

\begin_layout Standard
Il blocco 
\shape italic
Spark DataSource 
\shape default
fornisce un punto di raccordo tra i differenti 
\shape italic
datasource
\shape default
 che possono essere utilizzati all'interno del sistema e le librerie presenti
 nello
\shape italic
 Spark Core layer.
 
\shape default
Grazie all'utilizzo delle API messe a disposizione da questo blocco è possibile
 comunicare, senza dovere implementare chiamate diverse per 
\shape italic
datasource
\shape default
 diversi, con i più comuni 
\shape italic
datasource
\shape default
 attualmente utilizzati come 
\shape italic
Apache Hadoop
\shape default
, 
\shape italic
Cassandra, Hive, MySql, JSON, csv 
\shape default
e 
\shape italic
elasticSearch.
\end_layout

\begin_layout Standard
Il 
\shape italic
Machine Learning Model Layer 
\shape default
è il blocco che costituisce un 
\shape italic
wrapper
\shape default
 per il blocco 
\shape italic
Spark Core.
 
\shape default
La necessità di questo 
\shape italic
wrapper
\shape default
 nasce dall'esigenza di rendere il sistema indipendente dall'
\shape italic
engine 
\shape default
utilizzato per il processamento dei dati e per l'implementazione degli algoritmi
 di ML.
 In questo modo è possibile togliere e aggiungere 
\shape italic
engine
\shape default
 senza dovere stravolgere l'architettura del sistema.
 Questo incapsulamento è ottenuto tramite la costruzione di oggetti che
 rappresentano un'astrazione dei modelli di ML: la vera implementazione
 del modello sarà poi delegata all'
\shape italic
engine
\shape default
 utilizzato nel sistema.
\end_layout

\begin_layout Standard
Il 
\shape italic
System Data Layer 
\shape default
svolge una doppia funzionalità: la prima è quella di fornire il punto di
 accesso al lato di persistenza del sistema (salvataggio dei modelli e salvatagg
io dei 
\shape italic
task
\shape default
 di ML) la seconda è quella di essere un 
\shape italic
wrapper
\shape default
 per lo 
\shape italic
Spark DataSource.
 
\shape default
Anche in questo caso la scelta di creare un 
\shape italic
wrapper
\shape default
 per il layer 
\shape italic
Spark DataSource
\shape default
 è stata dettata dal bisogno di svincolare il sistema dall'utilizzo di un
 unico 
\shape italic
engine
\shape default
 e dalle sue API per la gestione dei diversi
\shape italic
 datasource.
\end_layout

\begin_layout Standard
L'ultimo blocco, il
\shape italic
 Machine Learning Task Layer
\shape default
, è il punto di entrata del sistema.
 Questo blocco si occupa di gestire i processi che portano alla creazione
 dei modelli e della fase di predizione nonché della delegazione delle operazion
i ai blocchi sottostanti.
\end_layout

\begin_layout Section
Moduli del sistema
\end_layout

\begin_layout Standard
Per l’architettura di questo 
\shape italic
framework
\shape default
 sono stati individuati i moduli rappresentati in figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Moduli-del-sistema"

\end_inset

.
 I moduli presenti nello schema forniscono le seguenti funzionalità:
\begin_inset Float figure
placement h
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/System sub-modules.png
	scale 50

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Moduli-del-sistema"

\end_inset

Moduli del sistema
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Itemize

\shape italic
Application Manager
\shape default
: costituisce l
\shape italic
'entry point
\shape default
 dell'applicazione.
 Si occupa di gestire la logica dei processi di creazione del modello e
 della fase di predizione;
\end_layout

\begin_layout Itemize

\shape italic
Prediction Manager
\shape default
: modulo per la gestione della fase di predizione;
\end_layout

\begin_layout Itemize

\shape italic
Models Manager
\shape default
: modulo per la gestione dei modelli di ML.
 Questo modulo si occupa di gestire il ciclo di vita di un modello;
\end_layout

\begin_layout Itemize

\shape italic
Models Plan Manager
\shape default
: gestione del processo di scelta, tra i modelli implementati nel sistema,
 del modello migliore per il problema di apprendimento automatico passato
 in 
\shape italic
input
\shape default
 al sistema;
\end_layout

\begin_layout Itemize

\shape italic
Spark Manager:
\shape default
 gestisce la connessione all'
\shape italic
engine
\shape default
 di 
\shape italic
Spark;
\end_layout

\begin_layout Itemize

\shape italic
Machine Learning Mlib
\shape default
: modulo per la gestione dell'implementazione degli algoritmi di ML messi
 a disposizione dalla libreria 
\shape italic
MLib
\shape default
 di 
\shape italic
Spark;
\end_layout

\begin_layout Itemize

\shape italic
DataFrameManager
\shape default
: gestione del processo di creazione di un 
\shape italic
dataframe;
\end_layout

\begin_layout Itemize

\shape italic
Spark DataSource Manager
\shape default
: manager dei 
\shape italic
datasource
\shape default
 utilizzati dall'
\shape italic
engine
\shape default
 di 
\shape italic
Spark;
\end_layout

\begin_layout Itemize

\shape italic
DataSource Manager
\shape default
: modulo che si occupa di gestire i diversi 
\shape italic
datasource
\shape default
, il salvataggio dei modelli e il salvataggio dei 
\shape italic
task
\shape default
 di 
\shape italic
Machine Learning
\shape default
;
\begin_inset VSpace smallskip
\end_inset


\end_layout

\begin_layout Standard
Per comprendere al meglio come i vari blocchi e i relativi moduli interagiscono
 tra di loro, nelle figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Comunication Diagram - Creazione nuovo Modello"

\end_inset

 e 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Comunication Diagram - Creazione predizione"

\end_inset

 sono mostrati i
\shape italic
 comunication diagram
\shape default
 
\begin_inset CommandInset citation
LatexCommand citep
key "key-19"

\end_inset

 relativi allo scambio di messaggi per la scelta di un modello di ML e per
 la creazione di una fase di predizione.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename schemi/ComunicationDiagramCreazioneModello.png
	scale 70
	rotateAngle 90

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Comunication Diagram - Creazione nuovo Modello"

\end_inset


\shape italic
Comunication Diagram
\shape default
 - Scelta di un modello
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename schemi/ComunicationDiagramaCreazionePredizione.png
	scale 70
	rotateAngle 90

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Comunication Diagram - Creazione predizione"

\end_inset


\shape italic
Comunication Diagram
\shape default
 - Creazione fase di predizione
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
