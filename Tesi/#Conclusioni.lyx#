#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass classicthesis
\begin_preamble

\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding default
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Conclusioni
\end_layout

\begin_layout Standard
Lo scopo del sistema sviluppato non è stato quello di supportare in maniera
 ottimale tutti i possibili scenari di 
\shape italic
Machine Learning,
\shape default
 ma di fornire delle buone 
\shape italic
performance
\shape default
 per la maggior parte degli scenari d'uso.
 Questo comportamento
\shape italic
 
\shape default
è una situazione comune che si riscontra anche in altri contesti, uno di
 questi è il caso dei 
\shape italic
Database Managment System
\shape default
 (DBMS).
 In questo contesto l'utilizzo di soluzioni ottimizzate per il linguaggio
 C++ forniscono dei buoni risultati sulla maggior parte delle 
\shape italic
query,
\shape default
 ma se si vogliono ottenere dei risultati migliori è necessario agire ad
 un livello di programmazione più basso e con l'aiuto di esperti del dominio
\begin_inset CommandInset citation
LatexCommand citep
key "key-35"

\end_inset

.
 Allo stesso modo, per trovare la soluzione ottima ad un problema di apprendimen
to automatico, è necessario effettuare una serie di operazioni che, in maniera
 chirurgica, vadano ad affrontare lo studio del 
\shape italic
dataset
\shape default
 attraverso la combinazione di tecniche come le 
\shape italic
features extraction
\shape default
 o l'applicazione di un determinato algoritmo.
 
\end_layout

\begin_layout Standard
Per cercare di venire incontro a quei problemi di ML che non restituiscono
 dei buoni risultati attraverso l'utilizzo del 
\shape italic
framework
\shape default
, parte dello sviluppo dell'architettura del sistema si è concentrata nel
 permettere l'integrazione di nuovi modelli di ML all'insieme di quelli
 già presenti nel sistema.
 In questo modo si cerca di estendere la classe dei problemi che il sistema
 può risolvere in maniera ottimale.
\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset

Un passo ulteriore che può essere compiuto per fornire un 
\shape italic
framework
\shape default
 ancora più performante consiste nell'andare a lavorare sui seguenti punti:
\end_layout

\begin_layout Itemize
preprocessamento dei modelli,
\end_layout

\begin_layout Itemize
ottimizzazione della scelta dei modelli,
\end_layout

\begin_layout Itemize
aumento capacità 
\shape italic
hardware
\shape default
.
\end_layout

\begin_layout Standard
Il primo punto è di estrema importanza al fine di predire risultati migliori.
 Al raggiungimento di questo scopo sono già state attuate, nella prima versione
 del software, alcune tecniche, come le normalizzazione delle 
\shape italic
feature
\shape default
, che permettono di ottenere delle predizioni più accurate.
 Lavorando in questa direzione è possibile aggiungere, a scapito del tempo
 di esecuzione della creazione di un modello di ML, l'applicazione della
 
\shape italic
Principal Component Analysis
\shape default
 (PCA) al 
\shape italic
dataset
\shape default
.
 Questa tecnica consiste nel proiettare i vettori in uno spazio dimensionale
 più piccolo di quello in 
\shape italic
input
\shape default
 riducendo il numero di 
\shape italic
features
\shape default
 che deve essere considerato durante la fase di 
\shape italic
training
\shape default

\begin_inset CommandInset citation
LatexCommand citep
key "key-36"

\end_inset

.
\end_layout

\begin_layout Standard
Il secondo punto riguarda la diminuzione del tempo di esecuzione della scelta
 del modello migliore.
 L'approccio implementato in questa versione si basa sull'analizzare solamente
 una porzione del 
\shape italic
dataset
\shape default
 e, in base al 
\shape italic
training
\shape default
 dei modelli su quest'ultima, viene scelto il modello migliore.
 Naturalmente l'adozione di questa strategia porta a non considerare il
 
\shape italic
dataset
\shape default
 nella sua totalità e quindi a scartare delle istanze che possono essere
 significative al fine dell'apprendimento.
 Una delle possibili soluzioni a questo problema può essere quella proposta
 in 
\begin_inset CommandInset citation
LatexCommand citep
key "key-38"

\end_inset

 nella quale vengono eseguite delle euristiche sul tempo di esecuzione del
 
\shape italic
training
\shape default
 dei modelli.
 In questo modo è possibile scegliere il modello che impiegherà il minor
 tempo, in base al valore dei parametri dell'algoritmo utilizzato nel modello
 e all'applicazione delle tecniche per le 
\shape italic
features extraction
\shape default
, per essere costruito.
\end_layout

\begin_layout Standard
Come è stato evidenziato nel capitolo 
\begin_inset CommandInset ref
LatexCommand ref
reference "chap:Analisi-Performance"

\end_inset

, l'utilizzo di 
\shape italic
Spark
\shape default
 si basa fortemente sulla memoria messa a disposizione nel 
\shape italic
cluster
\shape default
.
 Aumentare la RAM a disposizione del 
\shape italic
cluster
\shape default
 significa aumentare le capacità di processamento del 
\shape italic
dataset
\shape default
 in memoria principale.
 Questa opzione deve essere considerata in virtù dell'utilizzo di 
\shape italic
Spark
\shape default
: cambiare 
\shape italic
engine 
\shape default
potrebbe significare spostare l'ago della bilancia delle 
\shape italic
performance
\shape default
 su un altro componente 
\shape italic
hardware
\shape default
 diverso da quello della RAM.
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard
La combinazione di !Chaos con il sistema di 
\shape italic
Machine Learning
\shape default
 sviluppato può essere applicata, oltre che nel campo dell'
\shape italic
High Energy Physics
\shape default
, in tutti quei contesti che coinvolgono il controllo di una grande quantità
 di componenti o 
\shape italic
devices.
 
\shape default
Un altro esempio molto rilevante sul quale può essere utilizzata l'integrazione
 di questi due sistemi è il controllo di una 
\shape italic
SmartCity
\shape default
 associata con il concetto dell'
\shape italic
Internet of Things
\begin_inset CommandInset citation
LatexCommand citep
key "key-67"

\end_inset


\shape default

\begin_inset CommandInset nomenclature
LatexCommand nomenclature
symbol "IoT"
description "Internet of Things"

\end_inset

(IoT)
\shape italic
.
 
\shape default
L'
\shape italic
Internet of Things
\shape default
, utilizzato all'interno delle 
\shape italic
SmartCity, 
\shape default
fornisce un accesso facilitato ad una grande varietà di 
\shape italic
devices
\shape default
 come le 
\shape italic
home appliances, 
\shape default
le telecamere di sicurezza, sensori di monitoraggio, attuatori, 
\shape italic
displays
\shape default
, veicoli, ecc..
 permettendo lo sviluppo di applicazioni che, sfruttando i dati prodotti
 da quest'ultimi, implementano dei servizi 
\begin_inset Quotes eld
\end_inset

intelligenti
\begin_inset Quotes erd
\end_inset

 come l'
\shape italic
home automation
\shape default
, la gestione intelligente dell'energia e la gestione del traffico.
 Questo scenario offre un terreno fertile per l'applicazione di tecniche
 di 
\shape italic
Machine Learning
\shape default
 all'interno di un sistema di controllo.
 I servizi messi a disposizione da !Chaos coadiuvati dai protocolli di comunicaz
ione dell'IoT forniscono l'astrazione necessaria per la gestione di un grande
 insieme di componenti, mentre l'utilizzo del 
\shape italic
Machine Learning
\shape default
 mette a disposizione uno strumento per la risoluzione dei problemi che
 sono stati elencati precedentemente.
\end_layout

\begin_layout Standard
In generale, l'applicazione di questi due sistemi fornisce un supporto in
 tutti quei contesti in cui è necessario attuare delle fasi decisionali
 e gestionali che conivolgono una grande quantità di dati e di componenti
 situati su ambienti distribuiti come nel caso delle 
\shape italic
SmarCity
\shape default
.
 In un mondo in cui l'ago della bilancia si sta spostando sempre più velocemente
 sui i big data e sulle infrastrutture distribuite, avere a disposizione
 degli strumenti come quelli elencati all'interno di questa tesi permette
 di migliorare e aggiungere funzionalità ai sistemi che vengono definiti
 come
\shape italic
 Expert System
\shape default
 o 
\shape italic
Intelligent System.
\end_layout

\end_body
\end_document
