public class ModelPlan {

public Model runPlan(Task task){

...

BlockingQueue<Message> queue = new ArrayBlockingQueue<>();

models.add(
	new SVMClassificationOfflineModel(task,queue));
models.add(
	new LogisticRegressionOfflineModel(task,queue));

ExecutorService executorService = Executors.newFixedThreadPool();

for (Model model : models) {

	executorService.execute(new Runnable() {
	    public void run() {
	       ModelSummary modelSummary = model.runTrain();
	       queue.add(modelSummary);
	    }
	});
    
}
executorService.shutdown();

return extractBestModelFromSummary(models,queue);
}
	...
}