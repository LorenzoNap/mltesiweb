public class ManagePrediction {

	public Prediction createPrediction(){

	ManageModel manageModel = new ManageModel(mlTask);
	Model model = manageModel.loadModel();
	...

	StreamingPredictionOfflineModel streamingPredictionOfflineModel = new StreamingPredictionOfflineModel(model,mlTask);
	return streamingPredictionOfflineModel;
	}

}
