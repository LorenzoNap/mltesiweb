LogisticRegression lr = new LogisticRegression();

ParamMap[] paramGrid = new ParamGridBuilder()
    .addGrid(lr.regParam(), new double[] {0.03,0.05,0.5,1})
    .addGrid(lr.maxIter(), new int[] {10})
    .addGrid(lr.fitIntercept())
    .addGrid(lr.elasticNetParam(), new double[] {0.8})
    .build();

TrainValidationSplit trainValidationSplit = new TrainValidationSplit()
    .setEstimator(lr)
    .setEvaluator(new BinaryClassificationEvaluator())
    .setEstimatorParamMaps(paramGrid)
    .setTrainRatio(0.8);    

TrainValidationSplitModel model = trainValidationSplit.fit(training);

model.transform(test);
return model.bestModel();    