public class ModelDescription {
	
	private String modelName; 

    private String modelType; 

    private ArrayList<String> featuresOfModel;

    private String status;

    private String dataSetFilePath;

    private String savedModelFilePath;

    private String modelTargetField;

    private HashMap<String,String> modelSummary;
    ...
}
