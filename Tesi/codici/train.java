public Object train(Object... args) {

    LinearRegression lr = new LinearRegression()
            .setMaxIter(10)
            .setRegParam(0.00003)
            .setElasticNetParam(0.8);

    // Fit the model
    LinearRegressionModel lrModel = lr.fit(dataFrame);

    return lrModel;
}