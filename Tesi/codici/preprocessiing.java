public Object preProcessing(Object... args) {
    
sparkConnectionUtils = SparkConnectionUtils.getInstance();
dataFrame = sparkConnectionUtils.
            read().
            json(getApplication().getModelDescription().getDataSetFilePath());

dataFrame = dataFrame.extractFeatures(getApplication().getFeatures());

return dataFrame;
}