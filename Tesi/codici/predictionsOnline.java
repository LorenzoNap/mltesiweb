public Object predictions(Object... args) {

JavaStreamingContext jsscTrainingData = new JavaStreamingContext(..);
JavaStreamingContext jsscInputPrediction = new JavaStreamingContext(..);


MyStreamingLinearRegressionWithSGD streamingLinearRegressionWithSGD = (MyStreamingLinearRegressionWithSGD) getModel()).getImplementedModel();

Thread threadTrain = new Thread(){
   public void run(){
        while(jsscTrainingData.getStream()){
        DataFrame dataFrame = sqlContext.jsonRDD(rdd);                   

        DataFrame predictions = (DataFrame) streamingLinearRegressionWithSGD.trainOn(dataFrame);

        }
    }
};

Thread threadPrediction = new Thread(){
   public void run(){
        while(jsscInputPrediction.getStream()){
        DataFrame dataFrame = sqlContext.jsonRDD(rdd);                   

        DataFrame predictions = (DataFrame) streamingLinearRegressionWithSGD.predictionOn(dataFrame);
        savePredictions(predictions);
        }
    }
};            


return true;
}