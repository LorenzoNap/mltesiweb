public Model loadModel(){
....
if(getModelType().equals(MLTesiConstants.MODEL_TYPE_REGRESSION)){

    Class<?> clazz = Class.forName(
        application.getModelDescription().getAlgorithm());
    Constructor<?> constructor = clazz.getConstructor(MLTask.class);
    RegressionOfflineModel regressionOfflineModel = 
        (RegressionOfflineModel) constructor.newInstance(application);
    regressionOfflineModel.loadModel();
    return  regressionOfflineModel;   
}
...
}