public Object postProcessing(Object... args) {

LinearRegressionTrainingSummary trainingSummary = lrModel.summary();

HashMap<String, String> summary = new HashMap<>();
summary.put("numIterations", trainingSummary.totalIterations());
summary.put("objectiveHistory", Vectors.dense(trainingSummary.objectiveHistory());
summary.put("r2", trainingSummary.r2());
summary.put("Coefficients",lrModel.coefficients());
summary.put("Intercept",lrModel.intercept());

getApplication().getModelDescription().setModelSummary(summary);

DBUtils.updateApplication(getApplication());

return null;
}