public class LinearRegressionOfflineModel extends RegressionOfflineModel{

@Override
public Object runTrain(Object... args) {

	boolean result = (boolean) preProcessing();
	if(result){
	    LinearRegressionModel lrModel = (LinearRegressionModel) train();
	    if(lrModel != null){
	        postProcessing(lrModel);
	        setImplementedModel(lrModel);
	    }
	}...

	return true;
}