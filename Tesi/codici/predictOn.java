public class LinearRegressionOfflineModel {
	public Object predictionOn(Object... args) {
	    LinearRegressionModel lrModel = (LinearRegressionModel) getImplementedModel();
	    return lrModel.transform((DataFrame) args[0]);
	}
}