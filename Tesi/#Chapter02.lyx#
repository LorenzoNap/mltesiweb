#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass classicthesis
\use_default_options true
\maintain_unincluded_children false
\language italian
\language_package default
\inputencoding default
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style plainnat
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Definizione dell’architettura del sistema
\end_layout

\begin_layout Standard
\begin_inset CommandInset label
LatexCommand label
name "ch:examples"

\end_inset

In questo capitolo verrà definita l’architettura del sistema e i principali
 moduli che ne faranno parte.
 Questo processo verrà gestito tenendo a mente i requisiti funzionali e
 di dominio del sistema.
\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Section
System Stack
\end_layout

\begin_layout Standard
In figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:System-Stack"

\end_inset

 è rappresentato il 
\shape italic
System Stack
\shape default
 del sistema che fornisce una panoramica generale dei principali moduli
 dell'architettura.
 Nello specifico:
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Spark Core
\series default
\shape default
: Il modulo Spark Core rappresenta il cuore dell'applicazione in quanto
 è l'
\shape italic
engine 
\shape default
utilizzato per il 
\shape italic
processing
\shape default
 dei 
\shape italic
dataset
\shape default
 e per l'implementazione dei modelli di Machine Learning.
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Spark DataSource API
\series default
\shape default
: Questo modulo fornisce le API per accedere ai diversi 
\shape italic
datasource
\shape default
 distribuiti e non utilizzati da Spark.
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Machine Learning Model
\series default
\shape default
: Modulo che si occupa del ciclo di vita dei modelli di apprendimento automatico
 all'interno del sistema.
\end_layout

\begin_layout Itemize

\series bold
\shape italic
System Data Layer
\series default
\shape default
: Modulo che si occupa della gestione dell'interazione tra il sistema e
 i diversi 
\shape italic
data sources
\shape default
 (filesystem distribuiti, database, ecc...).
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Machine Learning Task
\series default
\shape default
: Questo modulo rappresenta l'
\shape italic
entry point
\shape default
 del sistema e gestisce la logica per le varie operazioni messe a disposizione
 dal sistema.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename gfx/System Stack.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
System Stack
\begin_inset CommandInset label
LatexCommand label
name "fig:System-Stack"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset

La costruzione dell'architettura del sistema è avvenuta in maniera tale
 da rendere indipendente i moduli principali del sistema da quelli che caratteri
zzano l'utilizzo dell'
\shape italic
engine
\shape default
 di 
\shape italic
Apache Spark
\shape default
.
 In questo modo è possibile intercambiare diversi engine, o usarne uno implement
ato 
\shape italic
ex novo
\shape default
, senza dovere stravolgere l'architettura del sistema.
 
\end_layout

\begin_layout Section
Apache Spark
\end_layout

\begin_layout Standard
Il cuore del sistema si basa sull'utilizzo dell'
\shape italic
engine
\shape default
 di Apache Spark per il processamento di dataset su larga scala.
 La scelta di utilizzare questo 
\shape italic
engine
\shape default
 è dovuta
\begin_inset CommandInset citation
LatexCommand citep
key "key-3"

\end_inset

:
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Spark Performance
\series default
\shape default
: Spark ottiene delle 
\shape italic
performace
\shape default
 cento volete più veloci rispetto all'utilizzo di Apache Hadoop 
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Spark Datasource API
\series default
\shape default
: Spark mettere a disposizione delle APIs 
\begin_inset Quotes eld
\end_inset

easy-to-use
\begin_inset Quotes erd
\end_inset

 per operare su grandi dataset
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Unified Engine
\series default
\shape default
: Spark offre delle librerie di alto livello per il supporto di query SQL,
 streaming data, machine learning e graph processing.
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Machine Learning
\series default
\shape default
: Spark mette a disposizione una libreria scalibile di Machine Learning
 che implementa i più comuni algoritmi di apprendimento automatico.
\end_layout

\begin_layout Standard
L'utilizzo di Apache Spark ha permesso la creazione di un sistema in grado
 di sfruttare la potenza di un ambiente distribuito e scalato.
 In questo modo è possibile processare e creare modelli di machine learning
 usando la potenza di calcolo di computer cluster; anche rispetto a soluzioni
 comuni presenti in questo ambito come l'engine MapReduce di Apache Hadoop,
 come mostrato in figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Logistic Regression - Spark vs Hadoop"

\end_inset

.
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename gfx/logistic-regressionSparkVSHadoop.png
	scale 60

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Logistic Regression - Spark vs Hadoop"

\end_inset

Logistic Regression - Spark vs Hadoop
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Section
Requisiti del sistema
\end_layout

\begin_layout Standard
Prima di iniziare lo sviluppo dell’architettura del sistema è necessario
 comprendere e definire in maniera chiara e precisa quali sono i requisiti
 che il sistema deve soddisfare per ridurre gli errori durante la fase progettua
le dell’architettura.
 Avere una precisa dimensione dei requisiti del sistema obbliga l’archi-
 tetto a valutare in modo efficiente i suoi punti critici e a scegliere
 un’architettura che, qualora dovessero essere apportati dei cambiamenti
 durante la fase di sviluppo del software o dopo che l’applicazione fosse
 rilasciata, supporti, a basso costo, operazioni di modifica del sistema
 stesso.
 I requisiti principali che il sistema deve soddisfare sono i seguenti:
\end_layout

\begin_layout Description
Funzionali
\end_layout

\begin_layout Itemize
Creazione creare un modello di ML a partire da un 
\shape italic
dataset
\shape default
 dato in input al sistema
\end_layout

\begin_layout Itemize
Il sistema deve essere in grado di gestire 
\shape italic
dataset
\shape default
 distributi e quindi interfacciarsi con 
\shape italic
filesystem
\shape default
 distribuiti
\end_layout

\begin_layout Itemize
Il sistema deve essere in grado di salvare i modelli di ML su memoria persistent
e per potere essere riutilizzati
\end_layout

\begin_layout Itemize
Il sistema deve essere in grado di caricare un precedente modello creato
 e utilizzarlo per una successiva fase di predizione
\end_layout

\begin_layout Itemize
Il sistema deve essere in grado, durante la fase di predizione, di acquisire
 input da diverse sorgenti dati
\end_layout

\begin_layout Itemize
Il sistema deve essere in grado di salvare e caricare lo stato di un task
 di ML
\end_layout

\begin_layout Itemize
Il sistema deve essere in grado di visualizzare le predizioni in corso
\end_layout

\begin_layout Standard
Vediamo ora in dettaglio i requisiti espressi nei precedenti punti.
\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset


\end_layout

\begin_layout Standard
Il compito principale del sistema è quello di risolvere task di Machine
 Learning.
 Con un task di machine learning si intende un problema di apprendimento
 automatico risolvibile tramite l'implementazione di un algoritmo che genera
 un modello matematico il quale può effettuare delle predizioni su input
 futuri come descritto nella capitolo 
\begin_inset CommandInset ref
LatexCommand ref
reference "ch:Cap1-MachineLearning"

\end_inset

.
 Considerato che per un singolo problema esistono diverse soluzioni, ossia
 diversi modelli che possono risolvere lo stesso problema, il sistema deve
 essere in grado di scegliere il modello, tra quelli implementati all'interno
 del sistema stesso, che meglio si adatta al problema in input.
 La scelta di tale modello avverrà tramite una procedura automatica che
 in base a delle opportune metriche selezionerà il modello con il valore
 più alto di queste misure.
\end_layout

\begin_layout Standard
La fase preliminare per la costruzione di un modello di ML consiste nell'acquisi
zione e nel 
\shape italic
preprocessing 
\shape default
del dataset.
 Poichè uno degli obiettivi del sistema è quello di essere utilizzabile
 all'interno di ambienti distribuiti, il sistema avrà implementate alcune
 funzioni in grado di interagire con i comuni filesystem distribuiti come
 HDFS di apache Hadoop.
 Una volta acquisito e processato il data set è possibile iniziare la fase
 di costruzione e scelta del modello finale di ML.
 Durante questa fase verrano costruiti i modelli che possono essere utilizzati
 per risolvere il tipo di problema passato in input, come descritto nella
 sezione 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Creazione-del-modello"

\end_inset

.
 Una volta scelto il modello che meglio si adatta alla risoluzione del problema,
 questo viene salvato in memoria persistente in maniera tale da potere essere
 
\begin_inset Quotes eld
\end_inset

chiamato
\begin_inset Quotes erd
\end_inset

 ogni qual volta è necessario attuare una fase di predizione.
 
\end_layout

\begin_layout Standard
La fase di predizione consiste solamente nel caricare un modello precedentemente
 creato e utilizzarlo come funzione di predizione per gli input che vengono
 acquisti dalle diverse sorgenti dati con cui il sistema può interagire.
 Questo processo costruisce una fase di streaming predittivo online che
 restituisce le predizioni in maniera real-time, operazione molto utile
 nel caso di applicazioni che hanno un certo livello di criticità: un esempio
 di integrazione tra questi due sistemvera esaminata nella sezione (mettere
 sezione).
 L'output della predizione verrà salvato su udb per potere essere utilizzato
 dopo o da altre sorgenti (scriver emeglio)
\end_layout

\begin_layout Standard
Durante il processo di acquisizione del dataset, della creazione dei modelli,
 della scelta finale del modello e della predizione, il sistema deve essere
 in grado di salvare gli output delle varie operazioni in maniera tale da
 tenere traccia del task di ML.
 Ad ogni task sottomesso al sistema verranno associate due informazioni
 prinicipali: la prima descrive il modello finale associato a quel determinato
 task e la seconda descrive il processo di predizione in corso ( o l'ultimo
 eseguito) su quel determinato task.
\end_layout

\begin_layout Standard
L'ultima operazione che il sistema deve supportare riguarda la visualizzazione
 delle predizioni che sono attualmente attive.
 In questa maniera gli utenti che utilizzano il sistema possono 
\end_layout

\begin_layout Section
Un approccio modulare
\end_layout

\begin_layout Standard
Una volta che sono stati definiti i requisiti del sistema è possibile iniziare
 a specificare l’architettura del sistema.
 Per la costruzione dell’architettura è stato scelto un approccio che ne
 garantisse la modularità dei vari componenti in modo tale da renderli il
 più possibile indipendenti l’uno dall’altro.
 Questo principio, riportato nella programmazione ad oggetti, corrisponde
 alla loose coupling property
\begin_inset CommandInset citation
LatexCommand citep
key "key-5"

\end_inset

.
 Sviluppare l’architettura di un sistema con un approccio loosely coupled
 permette di progettare dei componenti che hanno o che fanno uso di una
 parte o di nessuna informazione riguardo alla definizione degli altri component
i del sistema
\begin_inset CommandInset citation
LatexCommand citep
key "key-7"

\end_inset

.
 Sviluppare il sistema secondo questa linea guida permette di creare dei
 moduli dove ognuno di essi ha il compito di eseguire solamente un insieme
 di operazioni che appartengono allo stesso insieme di funzioni collegate
 logicamente: in questo modo, la modifica o il cambiamento di un modulo
 avrà un basso impatto sulle dipendenze del modulo stesso e sull’intero
 sistema.
 Riassumendo, questa scelta architetturale oltre a definire in ambienti
 separati le funzionalità che devono essere espresse dal sistema, permette
 un isolamento dei componenti che, se in un eventuale futuro dovessero essere
 cambiati o modificati, comporterebbe un spreco minimo di risorse.
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename schemi/Moduli del sistema.png
	scale 50

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Moduli-del-sistema"

\end_inset

Moduli del sistema
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
Per l’architettura di questo framework sono stati individuati i moduli rappresen
tati in figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Moduli-del-sistema"

\end_inset

 , dove per ogni modulo sono presenti delle frecce che indicano la dipendenza
 dagli altri moduli del sistema.
 I moduli presenti nello schema forniscono le seguenti funzionalità:
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Models Manager 
\series default
\shape default
: Modulo che si occupa di gestire i modelli per l'apprendimento automatico.
 Questo modulo si occupa di gestire il ciclo di vita di un modello
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Application Manager
\series default
\shape default
: Modulo principale del sistema: rappresenta l'entry point dell'applicazione
 e si occupa di gestire la logica dei processi di creazione del modello
 e della fase di predizione.
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Database Manager
\series default
\shape default
: Modulo per l'interfacciamento con il database che si occupa di salvare
 e recuperare le informazioni relative ad un task di ML.
\end_layout

\begin_layout Itemize

\series bold
\shape italic
Distributed Filesystem
\series default
\shape default
: Modulo per la gestione dei filesystem distribuiti.
 In particolare questo modulo serve per interagire con l'accesso ai filesystem
 distribuiti.
\end_layout

\begin_layout Section
Diagrammi UML
\end_layout

\begin_layout Standard
Per definire meglio l’architettura del sistema è utile fornire alcuni diagrammi
 di interazione tra i mo- duli, presi in prestito dai metodi dell’ingegneria
 del software che facilitano una produzione di software di alta qualità
\begin_inset CommandInset citation
LatexCommand citep
key "key-9"

\end_inset

.
 I metodi utilizzati per la creazione di questi digrammi affrontano i problemi
 attraverso un approccio orientato al paradigma obejct oriented(OO) utilizzando
 un linguaggio di modellazione che porta il nome di Unified Model Language(UML).
 La scrittura di questi diagrammi aiuta a definire, in fase implementativa
 del sistema, quali saranno le classi e metodi che si dovranno sviluppare
 per rispet- tare l’architettura del sistema.
 L’aspetto più importante risiede nella focalizzazione dell’interazione
 che avviene tra i vari moduli obbligando lo sviluppatore a pensare ad una
 struttura che interagisca rispet- tando le interfacce che sono state definite
 in fase architetturale.
 Il primo diagramma che verrà illustrato viene chiamato Comunication Diagram
 che presta l’attenzione sullo scambio di oggetti e le relazioni tra i partecipa
nti allo scambio 
\begin_inset CommandInset citation
LatexCommand citep
key "key-10"

\end_inset

.
 In figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Comunication Diagram - Creazione nuovo Modello"

\end_inset

 è illustrato il comunication diagram relativo allo scambio di messaggi
 per l’esecuzione dell'operazione di creazione di un nuovo modello a partire
 da un dataset.
 
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename schemi/ComunicationDiagramCreazioneModello.png
	scale 50

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Comunication Diagram - Creazione nuovo Modello"

\end_inset

Comunication Diagram - Creazione nuovo Modello
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset VSpace defskip
\end_inset

In figura 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:Comunication Diagram - Creazione predizione"

\end_inset

, invece è mostrato il comunication diagrama relativo alla creazione della
 fase di predizione.
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename schemi/ComunicationDiagramaCreazionePredizione.png
	scale 59

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:Comunication Diagram - Creazione predizione"

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Marginal
status collapsed

\begin_layout Plain Layout
You can use these margins for summaries of the text body
\begin_inset ERT
status collapsed

\begin_layout Plain Layout


\backslash
dots
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
A New Section
\end_layout

\begin_layout Standard
Illo principalmente su nos.
 Non message 
\emph on
occidental
\emph default
 angloromanic da.
 Debitas effortio simplificate sia se, auxiliar summarios da que, se avantiate
 publicationes via.
 Pan in terra summarios, capital interlingua se que.
 Al via multo esser specimen, campo responder que da.
 Le usate medical addresses pro, europa origine sanctificate nos se.
\end_layout

\begin_layout Standard
Examples: 
\shape italic
Italics
\shape default
, 
\begin_inset Flex CT - Spaced All Caps
status collapsed

\begin_layout Plain Layout
all caps
\end_layout

\end_inset

, 
\shape smallcaps
Small Caps
\shape default
, 
\begin_inset Flex CT - Spaced Low Small Caps
status collapsed

\begin_layout Plain Layout
low small caps
\end_layout

\end_inset

.
\end_layout

\begin_layout Standard
Acronym test: 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
acs{UML}
\end_layout

\end_inset

 -- 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
acf{UML}
\end_layout

\end_inset

 -- 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
acfp{UML}
\end_layout

\end_inset

 
\end_layout

\begin_layout Subsection
Test for a Subsection
\end_layout

\begin_layout Standard
\begin_inset Marginal
status collapsed

\begin_layout Plain Layout
Note: The content of this chapter is just some dummy text.
 It is not a real language.
\end_layout

\end_inset

 Lorem ipsum at nusquam appellantur his, ut eos erant homero concludaturque.
 Albucius appellantur deterruisset id eam, vivendum partiendo dissentiet
 ei ius.
 Vis melius facilisis ea, sea id convenire referrentur, takimata adolescens
 ex duo.
 Ei harum argumentum per.
 Eam vidit exerci appetere ad, ut vel zzril intellegam interpretaris.
\end_layout

\begin_layout Standard
Errem omnium per, pro 
\begin_inset Flex CT - acronym
status collapsed

\begin_layout Plain Layout

UML
\end_layout

\end_inset

 congue populo ornatus cu, ex qui dicant nemore melius.
 No pri diam iriure euismod.
 Graecis eleifend appellantur quo id.
 Id corpora inimicus nam, facer nonummy ne pro, kasd repudiandae ei mei.
 Mea menandri mediocrem dissentiet cu, ex nominati imperdiet nec, sea odio
 duis vocent ei.
 Tempor everti appareat cu ius, ridens audiam an qui, aliquid admodum conceptam
 ne qui.
 Vis ea melius nostrum, mel alienum euripidis eu.
\end_layout

\begin_layout Standard
Ei choro aeterno antiopam mea, labitur bonorum pri no.
 His no decore nemore graecis.
 In eos meis nominavi, liber soluta vim cu.
\end_layout

\begin_layout Subsection
Autem Timeam
\end_layout

\begin_layout Standard
Nulla fastidii ea ius, exerci suscipit instructior te nam, in ullum postulant
 quo.
 Congue quaestio philosophia his at, sea odio autem vulputate ex.
 Cu usu mucius iisque voluptua.
 Sit maiorum propriae at, ea cum 
\begin_inset Flex CT - acronym
status collapsed

\begin_layout Plain Layout

API
\end_layout

\end_inset

 primis intellegat.
 Hinc cotidieque reprehendunt eu nec.
 Autem timeam deleniti usu id, in nec nibh altera.
\end_layout

\begin_layout Section
Another Section in This Chapter
\end_layout

\begin_layout Standard
Non vices medical da.
 Se qui peano distinguer demonstrate, personas internet in nos.
 Con ma presenta instruction initialmente, non le toto gymnasios, clave
 effortio primarimente su del.
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Uno il nomine integre, lo tote tempore anglo-romanic per, ma sed practic
 philologos historiettas.
\end_layout

\end_inset


\end_layout

\begin_layout Standard
Sia ma sine svedese americas.
 Asia 
\begin_inset CommandInset citation
LatexCommand citeauthor
key "bentley:1999"

\end_inset

 
\begin_inset CommandInset citation
LatexCommand citep
key "bentley:1999"

\end_inset

 representantes un nos, un altere membros qui.
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
De web nostre historia angloromanic.
\end_layout

\end_inset

 Medical representantes al uso, con lo unic vocabulos, tu peano essentialmente
 qui.
 
\end_layout

\begin_layout Description
\begin_inset Flex CT - Description Label
status collapsed

\begin_layout Plain Layout
Description-Label Test
\end_layout

\end_inset

: Illo secundo continentes sia il, sia russo distinguer se.
 Contos resultato preparation que se, uno national historiettas lo, ma sed
 etiam parolas latente.
 Ma unic quales sia.
 Pan in patre altere summario, le pro latino resultato.
 
\end_layout

\begin_layout Description
\begin_inset Flex CT - Description Label
status collapsed

\begin_layout Plain Layout
Basate americano sia
\end_layout

\end_inset

: Lo vista ample programma pro, uno europee addresses ma, abstracte intention
 al pan.
 Nos duce infra publicava le.
 Es que historia encyclopedia, sed terra celos avantiate in.
 Su pro effortio appellate, o.
\end_layout

\begin_layout Standard
\noindent
Tu uno veni americano sanctificate.
 Pan e union linguistic 
\begin_inset CommandInset citation
LatexCommand citeauthor
key "cormen:2001"

\end_inset

 
\begin_inset CommandInset citation
LatexCommand citep
key "cormen:2001"

\end_inset

 simplificate, traducite linguistic del le, del un denomination.
\end_layout

\begin_layout Subsection
Personas Initialmente
\end_layout

\begin_layout Standard
Uno pote summario methodicamente al, uso debe nomina hereditage ma.
 Iala rapide ha del, ma nos esser parlar.
 Maximo dictionario sed.
\end_layout

\begin_layout Subsubsection
A Subsubsection
\end_layout

\begin_layout Standard
Deler utilitate methodicamente con se.
 Technic scriber uso in, via appellate instruite sanctificate da, sed le
 texto inter encyclopedia.
 Ha iste americas que, qui ma tempore capital.
  
\begin_inset CommandInset citation
LatexCommand citet
key "dueck:trio"

\end_inset


\end_layout

\begin_layout aEnumerate (ClassicThesis)
Enumeration with small caps (alpha) 
\end_layout

\begin_layout aEnumerate (ClassicThesis)
Second item
\end_layout

\begin_layout Paragraph
A Paragraph Example
\end_layout

\begin_layout Standard
Uno de membros summario preparation, es inter disuso qualcunque que.
 Del hodie philologos occidental al, como publicate litteratura in web.
 Veni americano 
\begin_inset CommandInset citation
LatexCommand citeauthor
key "knuth:1976"

\end_inset

 
\begin_inset CommandInset citation
LatexCommand citep
key "knuth:1976"

\end_inset

 es con, non internet millennios secundarimente ha.
 Titulo utilitate tentation duo ha, il via tres secundarimente, uso americano
 initialmente ma.
 De duo deler personas initialmente.
 Se duce facite westeuropee web, 
\begin_inset Flex CT - auto cross-reference
status collapsed

\begin_layout Plain Layout

tab:example
\end_layout

\end_inset

 nos clave articulos ha.
\end_layout

\begin_layout Standard
Medio integre lo per, non 
\begin_inset CommandInset citation
LatexCommand citeauthor
key "sommerville:1992"

\end_inset

 
\begin_inset CommandInset citation
LatexCommand citep
key "sommerville:1992"

\end_inset

 es linguas integre.
 Al web altere integre periodicos, in nos hodie basate.
 Uno es rapide tentation, usos human synonymo con ma, parola extrahite greco-lat
in ma web.
 Veni signo rapide nos da.
 
\begin_inset Float table
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="4" columns="3">
<features rotate="0" booktabs="true" tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Flex CT - Table Headline
status collapsed

\begin_layout Plain Layout
labitur bonorum pri no
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Flex CT - Table Headline
status collapsed

\begin_layout Plain Layout
que vista
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
\begin_inset Flex CT - Table Headline
status collapsed

\begin_layout Plain Layout
human
\end_layout

\end_inset


\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
fastidii ea ius
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
germano
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
demonstratea
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
suscipit instructior
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
titulo
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
personas
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
quaestio philosophia
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
facto
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
demonstrated 
\begin_inset CommandInset citation
LatexCommand citeauthor
key "knuth:1974"

\end_inset


\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset Argument 1
status collapsed

\begin_layout Plain Layout
Autem timeam deleniti usu id
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "tab:example"

\end_inset

Autem timeam deleniti usu id.
 
\begin_inset CommandInset citation
LatexCommand citeauthor
key "knuth:1974"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Linguistic Registrate
\end_layout

\begin_layout Standard
Veni introduction es pro, qui finalmente demonstrate il.
 E tamben anglese programma uno.
 Sed le debitas demonstrate.
 Non russo existe o, facite linguistic registrate se nos.
 Gymnasios, e.
\begin_inset space \thinspace{}
\end_inset

g., sanctificate sia le, publicate 
\begin_inset Flex CT - auto cross-reference
status collapsed

\begin_layout Plain Layout

fig:example
\end_layout

\end_inset

 methodicamente e qui.
\end_layout

\begin_layout Standard
Lo sed apprende instruite.
 Que altere responder su, pan ma, i.
\begin_inset space \thinspace{}
\end_inset

e., signo studio.
 
\begin_inset Flex CT - auto cross-reference
status collapsed

\begin_layout Plain Layout

fig:example-b
\end_layout

\end_inset

 Instruite preparation le duo, asia altere tentation web su.
 Via unic facto rapide de, iste questiones methodicamente o uno, nos al.
 
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
enlargethispage{2cm}
\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Float figure
placement bth
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/example_1.jpg
	width 45line%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Asia personas duo.
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset space \quad{}
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/example_2.jpg
	width 45line%

\end_inset

 
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:example-b"

\end_inset

Pan ma signo.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/example_3.jpg
	width 45line%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Methodicamente o uno.
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset space \quad{}
\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename gfx/example_4.jpg
	width 45line%

\end_inset

 
\begin_inset Caption Standard

\begin_layout Plain Layout
Titulo debitas.
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset Argument 1
status collapsed

\begin_layout Plain Layout
Tu duo titulo debitas latente
\end_layout

\end_inset


\begin_inset CommandInset label
LatexCommand label
name "fig:example"

\end_inset

Tu duo titulo debitas latente.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
