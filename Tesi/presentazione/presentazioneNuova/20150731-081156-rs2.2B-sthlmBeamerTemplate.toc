\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {2}{Introduzione}{3}{0}{1}
\beamer@subsectionintoc {2}{1}{Big Data}{4}{0}{1}
\beamer@subsectionintoc {2}{2}{Machine Learning}{7}{0}{1}
\beamer@sectionintoc {3}{Framework di Machine Learning}{10}{0}{2}
\beamer@subsectionintoc {3}{1}{Task di Machine Learning}{12}{0}{2}
\beamer@sectionintoc {4}{Implementazione del Sistema}{17}{0}{3}
\beamer@sectionintoc {5}{Utilizzo del Framework}{25}{0}{4}
\beamer@subsectionintoc {5}{1}{Framework di ML \& !CHAOS}{28}{0}{4}
\beamer@subsectionintoc {5}{2}{Utilizzo del framework}{32}{0}{4}
\beamer@sectionintoc {6}{Conclusioni}{39}{0}{5}
