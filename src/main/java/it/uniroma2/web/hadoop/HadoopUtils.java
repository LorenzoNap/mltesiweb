package it.uniroma2.web.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Lorenzo on 11/03/16.
 */
public class HadoopUtils {




    public static ArrayList<String> loadDataSet() throws IOException, URISyntaxException {


        //1. Get the Configuration instance
        Configuration configuration = new Configuration();
        //2. Get the instance of the HDFS
        FileSystem hdfs = FileSystem.get(new URI("hdfs://192.168.143.91:9000"), configuration);
        //3. Get the metadata of the desired directory
        FileStatus[] fileStatus = hdfs.listStatus(new Path("hdfs://192.168.143.91:9000/user/Lorenzo/Dataset"));
        //4. Using FileUtil, getting the Paths for all the FileStatus
        Path[] paths = FileUtil.stat2Paths(fileStatus);
        //5. Iterate through the directory and display the files in it


        ArrayList<String> dataSets = new ArrayList<>();
        for(Path path : paths)
        {
            dataSets.add(path.toString());
        }

        return dataSets;
    }
}
