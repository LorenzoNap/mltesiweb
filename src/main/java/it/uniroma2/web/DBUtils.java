package it.uniroma2.web;

import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import com.mongodb.util.JSON;
import org.bson.Document;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Lorenzo on 11/03/16.
 */
public class DBUtils {


    public static Document getModelByName(String name) {
        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        MongoDatabase mongoDatabase = mongoClient.getDatabase("MLSystem");


        FindIterable<Document> iterable = mongoDatabase.getCollection("applications").find(new Document("modelDescription.modelName",name));

        ArrayList<Document> documents = new ArrayList<>();
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documents.add(document);
            }
        });

        mongoClient.close();
        if(documents.size() > 0){
            return documents.get(0);
        }
        return null;
    }

    public static ArrayList<Document> getModels() {


        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        MongoDatabase mongoDatabase = mongoClient.getDatabase("MLSystem");


        FindIterable<Document> iterable = mongoDatabase.getCollection("applications").find(new Document("modelDescription", new Document("$exists", true)));

        ArrayList<Document> documents = new ArrayList<>();
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documents.add(document);
            }
        });

        mongoClient.close();

        for (Document document: documents) {
            Date date = (Date) document.get("last_modified_date");
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyy");
            String result = df.format(date);
            document.put("last_modified_date", result);

            String algo = (String) ((Document)document.get("modelDescription")).get("algorithm");
            if(algo != null){
                algo = algo.split("\\.")[algo.split("\\.").length-1];
                ((Document)document.get("modelDescription")).put("algorithm",algo);
            }

        }

        return documents;
    }

    public static ArrayList<Document> getApplications() {
        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        MongoDatabase mongoDatabase = mongoClient.getDatabase("MLSystem");


        FindIterable<Document> iterable = mongoDatabase.getCollection("applications").find();

        ArrayList<Document> documents = new ArrayList<>();
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documents.add(document);
            }
        });

        mongoClient.close();
        return documents;
    }

    public static void saveJson(JSONObject json) {

        json.remove("_id");

        json.put("date",new Date());
        json.put("id", getNextSequence("jsonApplicationID"));
        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        MongoDatabase mongoDatabase = mongoClient.getDatabase("MLSystem");

        MongoCollection<Document> collection = mongoDatabase.getCollection("json");
        Document document = Document.parse(json.toString());
        collection.insertOne(document);
        mongoClient.close();
    }


    public static Object getNextSequence(String name) {

        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        DB mongoDatabase = mongoClient.getDB("MLSystem");

        DBCollection collection = mongoDatabase.getCollection("counters");
        BasicDBObject find = new BasicDBObject();
        find.put("_id", name);
        BasicDBObject update = new BasicDBObject();
        update.put("$inc", new BasicDBObject("seq", 1));
        DBObject obj =  collection.findAndModify(find, update);
        return obj.get("seq");
    }


    public static ArrayList<JSONObject> getJsons() {

        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        MongoDatabase mongoDatabase = mongoClient.getDatabase("MLSystem");

        MongoCursor<Document> iterable = mongoDatabase.getCollection("json").find().iterator();


        BasicDBList list = new BasicDBList();
        ArrayList<JSONObject> documents = new ArrayList<>();


        while (iterable.hasNext()) {
            Document doc = iterable.next();
            list.add(doc);
            documents.add(new JSONObject(JSON.serialize(doc)));
        }

        mongoClient.close();


        return documents;
    }

    public static JSONObject getJson(int id) {
        MongoClientURI uri = new MongoClientURI("mongodb://admin:admin@192.168.143.91:27017/?authSource=admin");
        MongoClient mongoClient = new MongoClient(uri);

        MongoDatabase mongoDatabase = mongoClient.getDatabase("MLSystem");


        MongoCursor<Document> iterable = mongoDatabase.getCollection("json").find(new Document("id", id)).iterator();

        BasicDBList list = new BasicDBList();
        ArrayList<JSONObject> documents = new ArrayList<>();


        while (iterable.hasNext()) {
            Document doc = iterable.next();
            list.add(doc);
            documents.add(new JSONObject(JSON.serialize(doc)));
        }

        mongoClient.close();

        return documents.get(0);
    }
}