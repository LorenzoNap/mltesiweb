package it.uniroma2.web.utils;

import redis.clients.jedis.Jedis;

/**
 * Created by Lorenzo on 05/04/16.
 */
public class JedisUtils {


    private static JedisUtils instance = null;

    private Jedis jedis;

    protected JedisUtils() {
        jedis = new Jedis("192.168.143.92");
    }
    public static JedisUtils getInstance() {
        if(instance == null) {
            instance = new JedisUtils();
        }
        return instance;
    }

    public void seJedisValue(String appName, String value){
        jedis.set(appName, value);
    }

    public String getJedisValue(String appName){
       return jedis.get(appName);
    }
}
