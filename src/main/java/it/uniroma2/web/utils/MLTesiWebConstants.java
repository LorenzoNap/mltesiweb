package it.uniroma2.web.utils;

/**
 * Created by Lorenzo on 09/04/16.
 */
public class MLTesiWebConstants {


    public static final String REST_SERVICE_START_TASK = "startTask";
    public static final String REST_SERVICE_KILL_PREDICTION = "killPrediction";
}
