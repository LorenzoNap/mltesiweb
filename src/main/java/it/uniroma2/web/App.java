package it.uniroma2.web;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import it.uniroma2.web.hadoop.HadoopUtils;
import it.uniroma2.web.utils.JedisUtils;
import it.uniroma2.web.utils.MLTesiWebConstants;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.*;
/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        final String[] appName = {""};

        port(9090);
        staticFileLocation("/public");

        get("/", (request, response) -> {
            Map<String, Object> model = new HashMap<>();


            // The wm files are located under the resources directory
            return new ModelAndView(model, "index.vm");
        }, new VelocityTemplateEngine());

        get("/getAppInformation", (request, response) -> {
            if(appName[0] != null && !appName[0].equals("")){
                String appStatus = JedisUtils.getInstance().getJedisValue(appName[0]);
                if(appStatus == null){
                    return "";
                }
                return appStatus;
            }

            return "";


        });

        get("/killPrediction", (request, response) -> {
            if(appName[0] != null && !appName[0].equals("")){
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("application_name",appName[0]);
                callServer(jsonObject,MLTesiWebConstants.REST_SERVICE_KILL_PREDICTION);
            }

            return "";
        });

        get("/submitTask/:id", (request, response) -> {
            Map<String, Object> model = new HashMap<>();

            int id = Integer.decode(request.params(":id"));
            JSONObject jsonObject = DBUtils.getJson(id);

            appName[0] = (String) jsonObject.get("application_name");

            String template = "";
            if(jsonObject.get("task").equals("construct_model")){
                template = "models.vm";
            }else{
                template = "predictions.vm";
            }

            System.out.println(jsonObject.toString());
            model.put("response",callServer(jsonObject, MLTesiWebConstants.REST_SERVICE_START_TASK));
            model.put("json",jsonObject);

            DBUtils.saveJson(jsonObject);

            return new ModelAndView(model, template);
        },new VelocityTemplateEngine());


        //Pagina per la gestione dei Modelli
        get("/prediction/:action", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            //Create Model
            if(request.params(":action").equals("create")){
                model.put("action","create");
                ArrayList<String> dataSets = null;
                try {
                    dataSets = HadoopUtils.loadDataSet();
                    model.put("data_sets", dataSets);
                    model.put("models", DBUtils.getModels());
                    model.put("applications", DBUtils.getApplications());
                    return new ModelAndView(model, "predictions.vm");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

            } else if(request.params(":action").equals("status")){
                model.put("action","status");
                model.put("models", DBUtils.getModels());
                return new ModelAndView(model, "predictions.vm");

            }

            return new ModelAndView(model, "predictions.vm");

        }, new VelocityTemplateEngine());


        get("/history", (request, response) -> {
            Map<String, Object> model = new HashMap<>();

            model.put("jsons",DBUtils.getJsons());


            return new ModelAndView(model, "history.vm");
        }, new VelocityTemplateEngine());


        //Pagina per la gestione dei Modelli
        get("/models/:action", (request, response) -> {
            Map<String, Object> model = new HashMap<>();

            //Create Model
            if(request.params(":action").equals("create")){
                model.put("action","create");
                ArrayList<String> dataSets = null;
                try {
                    dataSets = HadoopUtils.loadDataSet();
                    model.put("data_sets", dataSets);
                    model.put("models", DBUtils.getModels());
                    return new ModelAndView(model, "models.vm");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

            } else if(request.params(":action").equals("status")){
                model.put("action","status");
                model.put("models", DBUtils.getModels());
                return new ModelAndView(model, "models.vm");

            }


            return new ModelAndView(model, "models.vm");
        }, new VelocityTemplateEngine());



        get("/create_prediction", (request, response) -> {
            Map<String, Object> model = new HashMap<>();

            if(request.queryParams("json") != null){
                JSONObject jsonObject = new JSONObject(request.queryParams("json"));
                System.out.println(jsonObject.toString());
                model.put("response",callServer(new JSONObject(request.queryParams("json")), MLTesiWebConstants.REST_SERVICE_START_TASK));
                model.put("json",jsonObject);
                appName[0] = jsonObject.get("application_name").toString();
                DBUtils.saveJson(jsonObject);


                return new ModelAndView(model, "predictions.vm");
            }else{
                String taskType = request.queryParams("task").toString();

                JSONObject jsonObject = new JSONObject();

                if(taskType.equals("prediction_with_model_offline")){
                    jsonObject.put("application_name",request.queryParams("application_name").toString());

                    appName[0] = request.queryParams("application_name").toString();


                    jsonObject.put("task",request.queryParams("task").toString());

                    //Prendi il modello
                    org.bson.Document mlModel = DBUtils.getModelByName(request.queryParams("model").toString());
                    org.bson.Document mlModelJson = (org.bson.Document) mlModel.get("modelDescription");

                    JSONObject prediction = new JSONObject();
                    prediction.put("type","streaming");
                    prediction.put("model_type",mlModelJson.get("modelType").toString());
                    prediction.put("streaming_data_ip",request.queryParams("streaming_data_ip"));
                    prediction.put("streaming_data_port",request.queryParams("streaming_data_port"));
                    prediction.put("streaming_data_duration",request.queryParams("streaming_data_duration"));
                    prediction.put("model_data_set_path",mlModelJson.get("savedModelFilePath"));
                    prediction.put("source_type",request.queryParams("source_type"));

                    jsonObject.put("prediction",prediction);
                    System.out.println(jsonObject.toString());
                    model.put("response",callServer(jsonObject, MLTesiWebConstants.REST_SERVICE_START_TASK));
                    model.put("json",jsonObject);

                    DBUtils.saveJson(jsonObject);

                }else {
                    jsonObject.put("application_name",request.queryParams("application_name").toString());


                    appName[0] = request.queryParams("application_name").toString();


                    jsonObject.put("task","prediction_with_model_online");

                    JSONObject mlModel = new JSONObject();
                    mlModel.put("type","online");
                    mlModel.put("type_of_problem",request.queryParams("type_of_problem"));
                    JSONArray featuresArray = new JSONArray();
                    String[] features =  request.queryParams("features").toString().split(",");
                    for(int i = 0; i < features.length ; i++){
                        featuresArray.put(features[i]);
                    }
                    mlModel.put("features",featuresArray);
                    mlModel.put("label",request.queryParams("tag"));
                    mlModel.put("model_data_set_path", "");
                    mlModel.put("streaming_data_ip",request.queryParams("streaming_data_ip_data"));
                    mlModel.put("streaming_data_port",request.queryParams("streaming_data_port_data"));
                    mlModel.put("streaming_data_duration",request.queryParams("streaming_data_duration_data"));

                    jsonObject.put("ml_model",mlModel);

                    JSONObject prediction = new JSONObject();
                    prediction.put("type","streaming");
                    prediction.put("model_type",request.queryParams("type_of_problem"));
                    prediction.put("model_data_set_path","");
                    prediction.put("streaming_data_ip",request.queryParams("streaming_data_ip_train"));
                    prediction.put("streaming_data_port",request.queryParams("streaming_data_port_train"));
                    prediction.put("streaming_data_duration",request.queryParams("streaming_data_duration_train"));
                    prediction.put("source_type",request.queryParams("source_type_streaming"));

                    jsonObject.put("prediction",prediction);

                    System.out.println(jsonObject.toString());

                    model.put("response",callServer(jsonObject, MLTesiWebConstants.REST_SERVICE_START_TASK));
                    model.put("json",jsonObject);

                    DBUtils.saveJson(jsonObject);
                }


            }
            return new ModelAndView(model, "predictions.vm");
        }, new VelocityTemplateEngine());

        get("/create_model", (request, response) -> {
            Map<String, Object> model = new HashMap<>();

            if(request.queryParams("json") != null){
                JSONObject jsonObject = new JSONObject(request.queryParams("json"));
                System.out.println(jsonObject.toString());
                int responseCall = callServer(jsonObject, MLTesiWebConstants.REST_SERVICE_START_TASK);
                model.put("response",responseCall);
                model.put("json",jsonObject);

                DBUtils.saveJson(jsonObject);

                return new ModelAndView(model, "models.vm");
            }

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("application_name",request.queryParams("application_name").toString());
            //UpdateApplication name
            appName[0] = request.queryParams("application_name").toString();

            jsonObject.put("task","construct_model");

            JSONObject mlModel = new JSONObject();
            mlModel.put("type","offline");
            mlModel.put("type_of_problem",request.queryParams("type_of_problem"));

            mlModel.put("model_data_set_path",request.queryParams("data_set"));

            JSONArray featuresArray = new JSONArray();
            String[] features =  request.queryParams("features").toString().split(",");
            for(int i = 0; i < features.length ; i++){
                 featuresArray.put(features[i]);
            }
            mlModel.put("features",featuresArray);
            if(request.queryParams("type_of_problem").equals("regression") || request.queryParams("type_of_problem").equals("classification")){
                mlModel.put("label",request.queryParams("tag"));
            }else{
                if(!"".equals(request.queryParams("tag_clustering"))){
                    mlModel.put("clustering_categories",Integer.valueOf(request.queryParams("tag_clustering")));
                }else{
                    mlModel.put("clustering_categories","");
                }
            }


            jsonObject.put("ml_model",mlModel);

            System.out.println(jsonObject.toString());
            model.put("response",callServer(jsonObject, MLTesiWebConstants.REST_SERVICE_START_TASK));
            model.put("json",jsonObject);

            DBUtils.saveJson(jsonObject);


            return new ModelAndView(model, "models.vm");
        }, new VelocityTemplateEngine());



    }


    public static int callServer(JSONObject object, String service){
        HttpResponse<String> responseFromServer;
        try {
            responseFromServer = Unirest.post("http://localhost:4567/"+service)
                    .header("content-type", "application/json")
                    .header("cache-control", "no-cache")
                    .header("postman-token", "bdec9d78-a1cb-69be-58f4-d1260c68f56c")
                    .body(object.toString())
                    .asString();
           return responseFromServer.getStatus();
        } catch (UnirestException e) {
            e.printStackTrace();
            return 0;
        }

    }
}
